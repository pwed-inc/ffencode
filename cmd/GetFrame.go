package main

import (
	"fmt"

	"gitlab.com/pwed-inc/ffencode"
)

func main() {
	ff := ffencode.Ffencode{
		FfmpegPath:  "ffmpeg",
		FfprobePath: "ffprobe",
	}

	duration := ff.GetDuration("sample.webm")

	frameSecond := duration / 10

	ff.GetFrame("sample.webm", "out/Frame.png", &ffencode.Profile{
		[]ffencode.FfArg{
			{"-ss", fmt.Sprint(frameSecond)},
			{"-vframes", "1"},
		},
	})
}
