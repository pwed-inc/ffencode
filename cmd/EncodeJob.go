package main

import (
	"fmt"

	"gitlab.com/pwed-inc/ffencode"
)

func main() {
	p := ffencode.Profile{
		"-vf":  "scale=320:-2",
		"-c:v": "av1",
		"-b:v": "20k",
		"-c:a": "opus",
		"-b:a": "10k",
	}

	f := ffencode.New()

	j := f.NewJob("sample.mp4", "jobOutput.webm", &p)

	err := j.Execute()

	if err != nil {
		fmt.Println(err)
	}
}
