package main

import (
	"gitlab.com/pwed-inc/ffencode"
	"log"
)

func main() {
	ff := ffencode.Ffencode{
		FfmpegPath:  "./ffmpeg",
		FfprobePath: "ffprobe",
	}

	dj := ffencode.DashJob{
		Input:    "sample.webm",
		Manifest: "out/manifest.go",
		VideoProfiles: map[ffencode.Output]*ffencode.Profile{
			"out/video_160x90_250k.webm":    newVideoProfile("160x90", "250k"),
			"out/video_320x180_500k.webm":   newVideoProfile("320x180", "500k"),
			"out/video_640x360_750k.webm":   newVideoProfile("640x360", "750k"),
			"out/video_1280x720_1500k.webm": newVideoProfile("1280x720", "1500k"),
		},
		AudioProfiles: map[ffencode.Output]*ffencode.Profile{
			"out/audio-128k.webm": {
				Args: []ffencode.FfArg{
					{
						Key:   "-c:a",
						Value: "libvorbis",
					},
					{
						Key:   "-b:a",
						Value: "128k",
					},
					{
						Key:   "-vn",
						Value: "",
					},
					{
						Key:   "-f",
						Value: "webm",
					},
					{
						Key:   "-dash",
						Value: "1",
					},
				},
			},
		},
	}

	log.Println(ff.MakeDashSequential(&dj))
}

func newVideoProfile(resolution, bitrate string) *ffencode.Profile {
	p := ffencode.Profile{
		Args: []ffencode.FfArg{
			{
				Key:   "-c:v",
				Value: "libvpx-vp9",
			},
			{
				Key:   "-s",
				Value: resolution,
			},
			{
				Key:   "-b:v",
				Value: bitrate,
			},
			{
				Key:   "-keyint_min",
				Value: "150",
			},
			{
				Key:   "-g",
				Value: "150",
			},
			{
				Key:   "-tile-columns",
				Value: "4",
			},
			{
				Key:   "-frame-parallel",
				Value: "1",
			},
			{
				Key:   "-an",
				Value: "",
			},
			{
				Key:   "-f",
				Value: "webm",
			},
			{
				Key:   "-dash",
				Value: "1",
			},
		},
	}
	return &p
}
