package main

import (
	"fmt"

	"gitlab.com/pwed-inc/ffencode"
)

func main() {
	ff := ffencode.New()

	err := ff.EasyTranscode("sample.mp4", "output.webm")
	if err != nil {
		fmt.Println(err)
	}
}
