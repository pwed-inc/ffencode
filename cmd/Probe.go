package main

import (
	"encoding/json"
	"fmt"
	"log"

	"gitlab.com/pwed-inc/ffencode"
)

func main() {
	ff := ffencode.Ffencode{
		FfmpegPath:  "ffmpeg",
		FfprobePath: "ffprobe",
	}

	probe, err := ff.Probe("sample.webm")
	if err != nil {
		log.Fatal(err)
	}

	out, _ := json.MarshalIndent(probe, "", "  ")

	fmt.Println(string(out))
}
