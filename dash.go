package ffencode

import (
	"fmt"
	"log"
	"os/exec"
)

//VP9_DASH_PARAMS="-tile-columns 4 -frame-parallel 1"
//
//ffmpeg -i input_video.y4m -c:v libvpx-vp9 -s 160x90 -b:v 250k -keyint_min 150 -g 150 ${VP9_DASH_PARAMS} -an -f webm -dash 1 video_160x90_250k.webm
//
//ffmpeg -i input_video.y4m -c:v libvpx-vp9 -s 320x180 -b:v 500k -keyint_min 150 -g 150 ${VP9_DASH_PARAMS} -an -f webm -dash 1 video_320x180_500k.webm
//
//ffmpeg -i input_video.y4m -c:v libvpx-vp9 -s 640x360 -b:v 750k -keyint_min 150 -g 150 ${VP9_DASH_PARAMS} -an -f webm -dash 1 video_640x360_750k.webm
//
//ffmpeg -i input_video.y4m -c:v libvpx-vp9 -s 640x360 -b:v 1000k -keyint_min 150 -g 150 ${VP9_DASH_PARAMS} -an -f webm -dash 1 video_640x360_1000k.webm
//
//ffmpeg -i input_video.y4m -c:v libvpx-vp9 -s 1280x720 -b:v 1500k -keyint_min 150 -g 150 ${VP9_DASH_PARAMS} -an -f webm -dash 1 video_1280x720_500k.webm

//ffmpeg -i input_audio.wav -c:a libvorbis -b:a 128k -vn -f webm -dash 1 audio_128k.webm
//ffmpeg \
//-f webm_dash_manifest -i video_160x90_250k.webm \
//-f webm_dash_manifest -i video_320x180_500k.webm \
//-f webm_dash_manifest -i video_640x360_750k.webm \
//-f webm_dash_manifest -i video_640x360_1000k.webm \
//-f webm_dash_manifest -i video_1280x720_500k.webm \
//-f webm_dash_manifest -i audio_128k.webm \
//-c copy -map 0 -map 1 -map 2 -map 3 -map 4 -map 5 \
//-f webm_dash_manifest \
//-adaptation_sets "id=0,streams=0,1,2,3,4 id=1,streams=5" \
//manifest.mpd

type Output string
type DashJob struct {
	Input         string
	VideoProfiles map[Output]*Profile
	AudioProfiles map[Output]*Profile
	Manifest      string
}

func (ff *Ffencode) MakeDash(dj *DashJob) error {
	args := []string{"-i", dj.Input}
	for out, prof := range dj.VideoProfiles {
		for _, arg := range prof.Args {
			if arg.Value == "" {
				args = append(args, arg.Key)

			} else {
				args = append(args, arg.Key, arg.Value)
			}
		}
		args = append(args, string(out))
	}
	for out, prof := range dj.AudioProfiles {
		for _, arg := range prof.Args {
			if arg.Value == "" {
				args = append(args, arg.Key)

			} else {
				args = append(args, arg.Key, arg.Value)
			}
		}
		args = append(args, string(out))
	}
	cmd := exec.Command(ff.FfmpegPath, args...)

	log.Println(cmd)

	out, err := cmd.Output()
	log.Println(out)

	var mpdArgs []string

	for out, _ := range dj.VideoProfiles {
		mpdArgs = append(
			mpdArgs,
			"-f",
			"webm_dash_manifest",
			"-i",
			string(out),
		)
		log.Println(out)
	}
	for out, _ := range dj.AudioProfiles {
		mpdArgs = append(
			mpdArgs,
			"-f",
			"webm_dash_manifest",
			"-i",
			string(out),
		)
		log.Println(out)
	}
	streams := ""
	mpdArgs = append(mpdArgs, "-c", "copy")
	for i := 0; i < len(dj.VideoProfiles); i++ {
		mpdArgs = append(mpdArgs, "-map", fmt.Sprint(i))
		streams = streams + fmt.Sprint(i, ",")
	}
	for i := 0; i < len(dj.AudioProfiles); i++ {
		mpdArgs = append(mpdArgs, "-map", fmt.Sprint(i+len(dj.VideoProfiles)))
	}
	adaptionSets := "id=0,streams=" + streams[:len(streams)-1]
	if len(dj.AudioProfiles) != 0 {
		adaptionSets = adaptionSets + " id=1,streams=" + fmt.Sprint(len(dj.VideoProfiles))
	}
	mpdArgs = append(
		mpdArgs,
		"-f",
		"webm_dash_manifest",
		"-adaptation_sets",
		adaptionSets,
		dj.Manifest,
	)

	mpdCMD := exec.Command(ff.FfmpegPath, mpdArgs...)

	log.Println(mpdCMD)

	out, err = mpdCMD.Output()

	log.Println(out)

	return err
}

func (ff *Ffencode) MakeDashSequential(dj *DashJob) error {
	for out, prof := range dj.VideoProfiles {
		args := []string{"-i", dj.Input}
		for _, arg := range prof.Args {
			if arg.Value == "" {
				args = append(args, arg.Key)

			} else {
				args = append(args, arg.Key, arg.Value)
			}
		}
		args = append(args, string(out))
		cmd := exec.Command(ff.FfmpegPath, args...)

		log.Println(cmd)
		cmd.Run()

	}
	for out, prof := range dj.AudioProfiles {
		args := []string{"-i", dj.Input}
		for _, arg := range prof.Args {
			if arg.Value == "" {
				args = append(args, arg.Key)

			} else {
				args = append(args, arg.Key, arg.Value)
			}
		}
		args = append(args, string(out))
		cmd := exec.Command(ff.FfmpegPath, args...)

		log.Println(cmd)
		cmd.Run()

	}

	var mpdArgs []string

	for out, _ := range dj.VideoProfiles {
		mpdArgs = append(
			mpdArgs,
			"-f",
			"webm_dash_manifest",
			"-i",
			string(out),
		)
		log.Println(out)
	}
	for out, _ := range dj.AudioProfiles {
		mpdArgs = append(
			mpdArgs,
			"-f",
			"webm_dash_manifest",
			"-i",
			string(out),
		)
		log.Println(out)
	}
	streams := ""
	mpdArgs = append(mpdArgs, "-c", "copy")
	for i := 0; i < len(dj.VideoProfiles); i++ {
		mpdArgs = append(mpdArgs, "-map", fmt.Sprint(i))
		streams = streams + fmt.Sprint(i, ",")
	}
	for i := 0; i < len(dj.AudioProfiles); i++ {
		mpdArgs = append(mpdArgs, "-map", fmt.Sprint(i+len(dj.VideoProfiles)))
	}
	adaptionSets := "id=0,streams=" + streams[:len(streams)-1] + " id=1,streams=" + fmt.Sprint(len(dj.VideoProfiles))
	mpdArgs = append(
		mpdArgs,
		"-f",
		"webm_dash_manifest",
		"-adaptation_sets",
		adaptionSets,
		dj.Manifest,
	)

	mpdCMD := exec.Command(ff.FfmpegPath, mpdArgs...)

	log.Println(mpdCMD)

	out, err := mpdCMD.Output()

	log.Println(out)

	return err
}
