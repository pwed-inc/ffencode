package ffencode

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os/exec"
)

type Format struct {
	Filename       string  `json:"filename"`
	NbStreams      uint    `json:"nb_streams"`
	NbPrograms     uint    `json:"nb_programs"`
	FormatName     string  `json:"format_name"`
	FormatLongName string  `json:"format_long_name"`
	StartTime      float32 `json:"start_time,string"`
	Duration       float32 `json:"duration,string"`
	Size           uint    `json:"size,string"`
	BitRate        uint    `json:"bit_rate,string"`
	ProbeScore     uint    `json:"probe_score"`
	Tags           Tags    `json:"tags"`
}

type Tags struct {
	Language string `json:"language,omitempty"`
	Encoder  string `json:"encoder,omitempty"`
}

type Disposition struct {
	Default         uint `json:"default"`
	Dub             uint `json:"dub"`
	Original        uint `json:"original"`
	Comment         uint `json:"comment"`
	Lyrics          uint `json:"lyrics"`
	Karaoke         uint `json:"karaoke"`
	Forced          uint `json:"forced"`
	HearingImpaired uint `json:"hearing_impaired"`
	VisualImpaired  uint `json:"visual_impaired"`
	CleanEffects    uint `json:"clean_effects"`
	AttachedPic     uint `json:"attached_pic"`
	TimedThumbnails uint `json:"timed_thumbnails"`
}

type Stream struct {
	Index              uint        `json:"index"`
	CodecName          string      `json:"codec_name"`
	CodecLongName      string      `json:"codec_long_name"`
	Profile            string      `json:"profile,omitempty"`
	CodecType          string      `json:"codec_type"`
	CodecTagString     string      `json:"codec_tag_string"`
	CodecTag           string      `json:"codec_tag"`
	SampleFmt          string      `json:"sample_fmt,omitempty"`
	SampleRate         uint        `json:"sample_rate,string,omitempty"`
	Channels           uint        `json:"channels,omitempty"`
	ChannelLayout      string      `json:"channel_layout,omitempty"`
	BitsPerSample      uint        `json:"bits_per_sample,omitempty"`
	Width              uint        `json:"width,omitempty"`
	Height             uint        `json:"height,omitempty"`
	CodedWidth         uint        `json:"coded_width,omitempty"`
	CodedHeight        uint        `json:"coded_height,omitempty"`
	ClosedCaptions     uint        `json:"closed_captions,omitempty"`
	HasBFrames         uint        `json:"has_b_frames,omitempty"`
	SampleAspectRatio  string      `json:"sample_aspect_ratio,omitempty"`
	DisplayAspectRatio string      `json:"display_aspect_ratio,omitempty"`
	PixFmt             string      `json:"pix_fmt,omitempty"`
	Level              int         `json:"level,omitempty"`
	FieldOrder         string      `json:"field_order,omitempty"`
	Refs               uint        `json:"refs,omitempty"`
	RFrameRate         string      `json:"r_frame_rate"`
	AvgFrameRate       string      `json:"avg_frame_rate"`
	TimeBase           string      `json:"time_base"`
	StartPts           uint        `json:"start_pts"`
	StartTime          float32     `json:"start_time,string"`
	Disposition        Disposition `json:"disposition"`
	Tags               Tags        `json:"tags,omitempty"`
}

type Probe struct {
	Format  Format   `json:"format"`
	Streams []Stream `json:"streams"`
}

func (ff *Ffencode) Probe(input string) (Probe, error) {
	var probe Probe = Probe{}
	args := []string{"-print_format", "json", "-show_format", "-v", "quiet", "-show_streams", "-i", input}

	cmd := exec.Command(ff.FfprobePath, args...)

	var outb, errb bytes.Buffer
	cmd.Stdout = &outb
	cmd.Stderr = &errb
	err := cmd.Run()
	if err != nil {
		log.Fatal(err, errb.String())
	}
	out := outb.String()
	fmt.Println(out)
	err = json.Unmarshal([]byte(out), &probe)
	return probe, err
}

func (ff *Ffencode) GetDuration(input string) float32 {
	vp, err := ff.Probe(input)
	if err == nil {
		return vp.Format.Duration
	}
	return 0
}
