package ffencode

import "os/exec"

func (ff *Ffencode) GetFrame(input, output string, profile *Profile) {
	args := []string{"-i", input}
	args = append(args, getArgs(profile)...)
	args = append(args, output)
	cmd := exec.Command(ff.FfmpegPath, args...)
	cmd.Run()
}
