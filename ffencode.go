package ffencode

import (
	"os/exec"
)

// Ffencode is an object used to initialise encoding jobs.
//   It contains the path to the required ffmpeg binaries.
type Ffencode struct {
	FfmpegPath, FfprobePath string
}

// EncodeJob is an object to define an encoding job.
type EncodeJob struct {
	ffencode *Ffencode
	Input,
	Output string

	*Profile
}

// Profile defines the resolution, codecs, and bitrates to use for the output file
type Profile struct {
	Args []FfArg
}

type FfArg struct {
	Key, Value string
}

// New creates a new Ffencode object and returns a pointer.
//   It assumes ffmpeg is in the PATH on your system
func New() *Ffencode {
	return &Ffencode{
		FfmpegPath:  "ffmpeg",
		FfprobePath: "ffprobe",
	}
}

// EasyTranscode will take an input path and output path and transcode using
//   ffmpeg's default settings for the given output type
func (f *Ffencode) EasyTranscode(input, output string) error {
	cmd := exec.Command(f.FfmpegPath, "-i", input, output)
	return cmd.Run()
}

// NewJob creates an object representing an encoding job to be run
func (f *Ffencode) NewJob(input, output string, profile *Profile) *EncodeJob {
	return &EncodeJob{
		f,
		input,
		output,
		profile,
	}
}

// Execute will run an EncodeJob
func (j *EncodeJob) Execute() error {
	params := []string{
		"-i",
		j.Input,
	}
	params = append(params, getArgs(j.Profile)...)
	params = append(params, j.Output)
	cmd := exec.Command(j.ffencode.FfmpegPath, params...)
	return cmd.Run()
}

func getArgs(profile *Profile) []string {
	var params []string
	for _, arg := range profile.Args {
		if arg.Value == "" {
			params = append(params, arg.Key)
		} else {
			params = append(params, arg.Key, arg.Value)
		}
	}
	return params
}
